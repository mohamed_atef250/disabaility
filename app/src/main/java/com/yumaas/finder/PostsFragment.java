package com.yumaas.finder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class PostsFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);

        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(getActivity(),new AddSocialPostFragment(),"AddSocialPostFragment");
            }
        });

        final TestingAdapter2 teachersAdapter = new TestingAdapter2(getActivity());


        final RecyclerView programsList = rootView.findViewById(R.id.recycler_view);
       programsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        programsList.setAdapter(teachersAdapter);


        return rootView;
    }
}