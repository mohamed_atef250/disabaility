package com.yumaas.finder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;




public class ChildsFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);



        final TestingAdapter teachersAdapter = new TestingAdapter(getActivity());


        final RecyclerView programsList = rootView.findViewById(R.id.recycler_view);
       programsList.setLayoutManager(new GridLayoutManager(getActivity(),2));
        programsList.setAdapter(teachersAdapter);


        return rootView;
    }
}