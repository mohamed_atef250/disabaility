package com.yumaas.finder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;


public class SliderAdapterBarber extends SliderViewAdapter<SliderAdapterBarber.SliderAdapterVH> {

    private Context context;
 String[] images={"https://www.parentmap.com/sites/default/files/styles/1180x660_scaled_cropped/public/2018-01/iStock-644051822_0.jpg?itok=nLtX1n6J"
         ,"https://www.unicef.org/eca/sites/unicef.org.eca/files/styles/two_column/public/UN040884.jpg?itok=Emx3VacT",
         "https://promedicahealthconnect.org/media/Toledo-Area-Resources-for-Children-with-Special-Needs-797x385.jpg",
         "https://i2.wp.com/rdlounge.com/wordpress/wp-content/uploads/2018/03/ThinkstockPhotos-858352780-3.jpg?resize=833%2C540&ssl=1"};
    public SliderAdapterBarber(Context context) {
        this.context = context;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hall_image, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {

        Picasso.get().load(images[position]).into(viewHolder.imageView);

    }

    @Override
    public int getCount() {
        return images==null?0:images.length;
    }

    public static  class SliderAdapterVH extends SliderViewAdapter.ViewHolder  {

        ImageView imageView;

        public SliderAdapterVH(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);

        }
    }
}